if (process.env.NODE_ENV == 'development') {
    require('dotenv').config();
}
const express = require('express');
const morgan = require('morgan');
const app = express();

// Settings
const port = process.env.PORT || 5000;

//middleware
app.use(morgan('dev'));

// Importing routes
const ApiRoutes = require('./Routes/Api');
// Routes
app.use(ApiRoutes);

app.listen(port, () => console.log(`Listening on port ${port}`));
