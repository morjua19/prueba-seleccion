const express = require('express');
const router = express.Router();
const GotController = require('../Controllers/GotController');


router.get('/api/characters', GotController.getCharacters);


router.get('/api/characters/:id', GotController.getCharacter);
//
router.post('/api/characters', GotController.save);

module.exports = router;
