const controller = {};
const GotRepository = require('../Repositories/GotRepository');
const clientRedis = require('../redis');


controller.save = (req, res) => {
    try {
        clientRedis.exists('init_app_got', async (err, reply) => {
            if (!reply) {//first
                clientRedis.set('init_app_got', 1);
                await GotRepository.saveCharacters();
            }
        });

        res.send({status: 1, message: 'Data saved or updated'});
    } catch (e) {
        console.log("error exception", e.message);
        res.status(400).json({state: 0, message: e.message});
    }
};


controller.getCharacters = async (req, res) => {
    let {prev, next} = req.query;
    console.log(prev, next);
    try {
        let data = await GotRepository.getCharacters(prev, next);
        res.send({status: 1, message: 'Success data', data});
    } catch (e) {
        console.log("error exception", e.message);
        res.status(400).send({state: 0, message: e.message});
    }
};

controller.getCharacter = async (req, res) => {
    let {id} = req.params;

    try {
        let data = await GotRepository.getCharacter(id);
        res.send({status: 1, message: 'Success data', data});
    } catch (e) {
        console.log("Error exception", e.message);
        res.status(400).send({state: 0, message: e.message});
    }
};


module.exports = controller;
