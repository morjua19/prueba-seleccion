const repository = {};
const rp = require('request-promise');
const urlRestApi = `${process.env.URL_REST_API}`;
const clientRedis = require('../redis');
const {promisify} = require('util');
const getAsync = promisify(clientRedis.get).bind(clientRedis);
const getLRangeAsync = promisify(clientRedis.lrange).bind(clientRedis);


repository.saveCharacters = async () => {

    try {

        let urlFull = `${urlRestApi}/book/characters`;
        // console.log('Url Request: ', urlFull);
        let response = await rp(urlFull); //request
        response = JSON.parse(response);
        response.forEach((item) => {
            clientRedis.rpush(`got_characters`, JSON.stringify(item));
        });


        return {state: 1, message: 'Save Update success'};


    } catch (e) {
        console.log("Error exception", e.message);
        throw Error('An error occurred, request saveCharacters, ' + e.message);
    }
};

repository.getCharacter = async (id) => {

    try {

        let response = await getLRangeAsync(`got_characters`, 0, -1);

        return response.map(item => {
            return JSON.parse(item);
        }).find(item => (item.id === id));
    } catch (e) {
        console.log("Error exception", e.message);
        throw Error('An error occurred, request getCharacter, ' + e.message);
    }
};

repository.getCharacters = async () => {

    try {
        let response = await getLRangeAsync(`got_characters`, 0, -1);
        return response.map(item => {
            return JSON.parse(item);
        });

    } catch (e) {
        console.log("Error exception", e.message);
        throw Error('An error occurred, request getCharacters, ' + e.message);
    }
};

module.exports = repository;
